# vosk-models

[Vosk models](https://alphacephei.com/vosk/models) as `.tgz` instead of `.zip`.

---

```
./download.sh
./tar.sh
```

```
$ tree models/
models/
├── vosk-model-small-en-us-0.15.tgz
├── vosk-model-small-en-us-0.15.zip
├── vosk-model-small-es-0.42.tgz
└── vosk-model-small-es-0.42.zip
```
