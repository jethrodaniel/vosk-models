#!/usr/bin/env bash

cd models

for path in $(ls *.zip); do
  model=$(basename --suffix='.zip' $path)
  echo "$path -> $model.tgz"

  unzip $path
  tar czvf "$model.tgz" --owner=0 --group=0 "$model"
  rm -r $model
done
