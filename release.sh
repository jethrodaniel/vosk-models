#!/usr/bin/env bash

set -ex

release=$(date +%Y-%d-%m)

glab release create "$release" --name "$release" ./models/*.tgz
