#!/usr/bin/env bash

MODEL_URL=https://alphacephei.com/vosk/models/

curl "$MODEL_URL/model-list.json" |
  jq '.[] | select( .lang == "en-us" or .lang == "es") | select (.type == "small" and .obsolete == "false")' |
  jq '.url' |
  xargs wget --no-clobber --directory-prefix=models
